import { API_BASE_URL, ACCESS_TOKEN } from '../constants';
import moment from 'moment';

var fileDownload = require('js-file-download');

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
        // "Content-Type": "multipart/form-data"
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            }).catch(error => console.log('Error response to json ' + options.url + '. Msg: ' + error))
        ).catch(error => console.log('Error retrieving ' + options.url + '. Msg: ' + error));

};

const requestWithMultiPart = (options) => {
    const headers = new Headers({
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        )
};

const downloadRequest = (options, fileName) => {
    const headers = new Headers({
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }


    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.blob()).then(response => {
                fileDownload(response, fileName);

                console.log(response);
            }).


        catch(error => console.log('Error retrieving ' + options.url + '. Msg: ' + error));;
};


export function getCurrentUser() {

    if (!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.");
    }

    return request({
        url: API_BASE_URL + "/user/me",
        method: 'GET'
    });
}

export function login(loginRequest) {
    return request({
        url: API_BASE_URL + "/auth/login",
        method: 'POST',
        body: JSON.stringify(loginRequest)
    });
}

export function signup(signupRequest) {
    return request({
        url: API_BASE_URL + "/auth/signup",
        method: 'POST',
        body: JSON.stringify(signupRequest)
    });
}


export function getAllOrders(id, date) {
    return request({
        url: API_BASE_URL + '/order/get/' + id + '/' + date,
        method: 'GET'
    });
}

export function confirmOrder(body) {
    return request({
        url: API_BASE_URL + '/order/confirm',
        method: 'POST',
        body: body
    });
}

export function deliverOrder(body) {
    return request({
        url: API_BASE_URL + '/order/deliver',
        method: 'POST',
        body: body
    });
}

export function inTransitOrder(body) {
    return request({
        url: API_BASE_URL + '/order/intransit',
        method: 'POST',
        body: body
    });
}

export function markOrderAsComplete(body) {
    return request({
        url: API_BASE_URL + '/order/complete',
        method: 'POST',
        body: body
    });
}

export function rejectOrder(body) {
    return request({
        url: API_BASE_URL + '/order/reject',
        method: 'POST',
        body: body
    });
}

export function createNewItem(form) {
    return requestWithMultiPart({
        url: API_BASE_URL + '/product/create/',
        method: 'PUT',
        body: form
    });
}


export function getItems(shopId) {
    return request({
        url: API_BASE_URL + '/product/get/' + shopId,
        method: 'GET',
    });
}


export function updateAvailability(productId, availability) {
    return request({
        url: API_BASE_URL + '/product/update/' + productId,
        method: 'POST',
        body: availability
    });
}


export function deleteProduct(productId) {
    return request({
        url: API_BASE_URL + '/product/delete/' + productId,
        method: 'POST'
    });
}

export function updateShopInfo(body) {
    return request({
        url: API_BASE_URL + '/partner/shop/update',
        method: 'POST',
        body: body
    });
}

export function uploadShopImage(body) {
    return requestWithMultiPart({
        url: API_BASE_URL + '/partner/shop/upload',
        method: 'PUT',
        body: body
    });
}

export function uploadShopAvailability(body) {
    return request({
        url: API_BASE_URL + '/partner/shop/update/availability',
        method: 'POST',
        body: body
    });
}

export function uploadDeliveryService(body) {
    return request({
        url: API_BASE_URL + '/partner/shop/update/delivery',
        method: 'POST',
        body: body
    });
}

export function updatePreparationTime(body) {
    return request({
        url: API_BASE_URL + '/partner/shop/update/preparationTime',
        method: 'POST',
        body: body
    });
}

export function getLatestUpdate(shopId, date) {
    return request({
        url: API_BASE_URL + '/report/' + shopId +'/' +date,
        method: 'GET',
    });
}

export function generateReport(shopId, date) {
    return request({
        url: API_BASE_URL + '/report/generate/' + shopId,
        method: 'POST',
        body: date
    });
}


export function downloadReport(shopId, date) {
    const dateF = moment(date, 'YYYYMMDD').format('MMM D YYYY');

    const fileName = String(shopId) + ' - ' + dateF + '.xlsx';
    return downloadRequest({
        url: API_BASE_URL + '/report/download/' + shopId + '/' + date,
        method: 'GET'
    }, fileName);
}