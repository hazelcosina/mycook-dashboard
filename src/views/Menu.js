import { Tab } from "bootstrap";
import React from "react";

import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Table,
    Row,
    Col,
    Modal,
    ModalBody,
    FormText,
    FormGroup,
    Label,
    Input,
    Button,
    Form
} from "reactstrap";
import CardSubtitle from "reactstrap/lib/CardSubtitle";
import { getItems, createNewItem, updateAvailability, deleteProduct } from 'helpers/API';
import { AuthContext } from "contexts/AuthContext";
import { IMAGE_URL, ERROR_REQUIRED } from "constants/index"
import { formatNumber } from 'helpers/utils';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import CardFooter from "reactstrap/lib/CardFooter";

const Menu = ({ list }) => {
    const { user } = React.useContext(AuthContext);

    const [previewModal, setPreviewModal] = React.useState(false);
    const [product, selectedItem] = React.useState({});

    const [hideLimit, setHideLimit] = React.useState(true);
    const [choicesList, setChoicesList] = React.useState([]);

    // Variation Modal Fields
    const [variationName, setVariationName] = React.useState('');
    const [variationNameError, setVariationNameError] = React.useState('');

    const [select, setSelect] = React.useState('Single');
    const [limit, setLimit] = React.useState('0');
    const [selectError, setSelectError] = React.useState('');

    const [choice, setChoice] = React.useState('');
    const [choiceError, setChoiceError] = React.useState('');

    const [addOnPrice, setAddOnPrice] = React.useState();
    const [addOnPriceError, setAddOnPriceError] = React.useState('');

    const [variationModal, setVariationModal] = React.useState(false);
    const [variationList, setVariationList] = React.useState([]);
    const [toUpdate, setToUpdate] = React.useState(false);

    // Main Fields
    const [name, setName] = React.useState('');
    const [nameError, setNameError] = React.useState('');
    const [price, setPrice] = React.useState('');
    const [priceError, setPriceError] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [descriptionError, setDescriptionError] = React.useState('');
    const [image1, setImage] = React.useState(null);
    const [imageName, setImageName] = React.useState('');
    const [imageError, setImageError] = React.useState('');

    const [availableItems, setAvailableItems] = React.useState([{}]);
    const [modal, setModal] = React.useState(false);
    const [outOfStockItems, setOutOfStockItems] = React.useState([{}]);
    const [activeModal, setActiveModal] = React.useState(false);
    const [productItem, setProductItem] = React.useState({});

    const [variationError, setVariationError] = React.useState(false);

    const handleNameChange = (event) => {
        setName(event.target.value);
        setNameError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handlePriceChange = (event) => {
        setPrice(event.target.value.replace(/[^\d+\.?\d*]/, ''));
        setPriceError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
        setDescriptionError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleImageChange = (event) => {
        setImage(event.target.files[0]);
        setImageName(event.target.value);
        setImageError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleVariationNameChange = (event) => {
        setVariationName(event.target.value);
        setVariationNameError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleSelectionChange = (event) => {
        setSelect(event.target.value);
        setHideLimit(select !== 'Single' ? true : false);
    };

    const handleLimitChange = (event) => {
        setLimit(event.target.value);
        setSelectError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleChoiceChange = (event) => {
        setChoice(event.target.value);
        setChoiceError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const handleAddOnPriceChange = (event) => {
        setAddOnPrice(event.target.value.replace(/[^\d+\.?\d*]/, ''));
        setAddOnPriceError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
    };

    const addChoice = () => {

        if (!validationChoiceInputs()) {
            const item = {
                choice: choice,
                price: addOnPrice
            };
            const newRows = [...choicesList, item]
            setChoicesList(newRows);

            // Set to default
            setChoice('');
            setAddOnPrice('');
        }
    };
    const removeChoice = (item) => {
        const newRows = choicesList.filter(row => row !== item);
        setChoicesList(newRows);
    };

    const addVariation = () => {

        if (!validateVariationInputs()) {
            if (choicesList.length > 1) {
                const newVariation = {
                    variation: variationName,
                    selection: select,
                    limit: limit,
                    choices: choicesList
                };

                if (toUpdate) {

                    let updated = variationList.filter(variation => variation.variation !== newVariation.variation);
                    updated = [...updated, newVariation]
                    setVariationList(updated);

                    alert('Variation is updated.');
                } else {
                    const newRows = [...variationList, newVariation]
                    setVariationList(newRows);
                    localStorage.setItem('unsavedVariations', JSON.stringify(newRows));

                    alert('Variation is saved.');
                }

                toggleVariationModal();
            } else {
                alert('You must enter atleast 2 choices');
            }
        }
    };

    const removeVariation = (item) => {
        const newRows = variationList.filter(row => row !== item);
        setVariationList(newRows);
    };


    React.useEffect(() => {
        loadItems();
    }, []);

    const validationChoiceInputs = () => {
        let hasErrors = false;

        if (!addOnPrice) {
            setAddOnPriceError(ERROR_REQUIRED);
            hasErrors = true;
        }
        if (!choice) {
            setChoiceError(ERROR_REQUIRED);
            hasErrors = true;
        }
        return hasErrors;

    }

    const validateVariationInputs = () => {
        let hasErrors = false;
        if (!variationName) {
            setVariationNameError(ERROR_REQUIRED);
            hasErrors = true;
        }
        if (select == 'Multiple' && !limit) {
            setSelectError(ERROR_REQUIRED);
            hasErrors = true;
        }
        return hasErrors;
    }


    const validateInputs = () => {
        let hasErrors = false;
        if (!name) {
            setNameError(ERROR_REQUIRED);
            hasErrors = true;
        }
        if (!description) {
            setDescriptionError(ERROR_REQUIRED);
            hasErrors = true;
        }
        if (!price) {
            setPriceError(ERROR_REQUIRED);
            hasErrors = true;
        }
        if (!image1) {
            setImageError(ERROR_REQUIRED);
            hasErrors = true;
        }
        return hasErrors;
    }
    const save = () => {

        if (!validateInputs()) {
            confirmAlert({
                customUI: ({ onClose }) => {

                    return (
                        <Col xs="9" className="mr-auto mx-auto">
                            <Card className="card-plain">
                                <h5>Are you finished creating this product?</h5>
                                <CardFooter className="mr-auto mx-auto p-4">
                                    <Button onClick={() => onClose} color="warning">Not yet</Button>
                                    <Button onClick={() => { mapToFormData(); onClose() }} color="success">Yup!</Button>
                                </CardFooter>
                            </Card>
                        </Col>

                    );
                }
            });
        }
    }

    const mapToFormData = () => {

        let variations = '';
        variationList.forEach((obj, i) => {

            // Add Options 
            let optionList = '';
            obj.choices.forEach((choice, i) => {
                let optionStr = choice.choice + '=' + choice.price
                optionList = optionList.concat(optionStr) + (i < obj.choices.length - 1 ? ',' : '');
            })

            const variation = obj.variation + ',' + obj.selection + ',' + obj.limit + ',' + optionList + (i < variationList.length - 1 ? ';' : '');
            variations = variations.concat(variation);
        })

        let formData = new FormData();
        formData.append('name', name);
        formData.append('description', description);
        formData.append('price', price);
        formData.append('image1', image1);
        formData.append('variations', variations);
        formData.append('isAvailable', false);

        createNewItem(formData).then(res => {
            loadItems();
            resetFields();
        },
            error => {
                alert(error);
            });
    }

    const loadItems = () => {
        getItems(user.userId).then(res => {
            if (res) {
                const available = res.filter(item => (item.available == true));
                setAvailableItems(available);

                const outOfStock = res.filter(item => (!item.available || item.available == false));
                setOutOfStockItems(outOfStock);
            }
        },
            error => {
                alert(error);
            });
    }

    const sendToOutOfStock = (productId) => {
        updateAvailability(productId, false).then(res => {
            loadItems();
            toggleModal(productItem, false);
        },
            error => {
                alert(error);
            });
    }

    const sendToAvailableItems = (productId) => {
        updateAvailability(productId, true).then(res => {
            loadItems();
            toggleModal(productItem, true);

        },
            error => {
                alert(error);
            });
    }

    const removeProduct = (productId) => {
        toggleModal(productItem, null);
        confirmAlert({
            customUI: ({ onClose }) => {

                return (
                    <Col xs="9" className="mr-auto mx-auto">
                        <Card className="card-plain">
                            <h5>Are you sure want to delete this product?</h5>
                            <CardFooter className="mr-auto mx-auto p-4">
                                <Button onClick={onClose} color="warning">Cancel</Button>
                                <Button onClick={() => { confirmDelete(productId); onClose() }} color="danger">Yup!</Button>
                            </CardFooter>
                        </Card>
                    </Col>

                );
            }
        });
    }

    const confirmDelete = (productId) => {
        deleteProduct(productId).then(res => {
            loadItems();
            toggleModal(productItem, null);

        },
            error => {
                alert(error);
            });
    }

    const toggleVariationModal = (variation) => {
        setVariationModal(!variationModal);

        if (variation) {
            setVariationName(variation.variation);
            setSelect(variation.selection);
            setChoicesList(variation.choices);
            setLimit(variation.limit);
            setToUpdate(true);
        } else {
            setVariationName('');
            setSelect('Single');
            setChoicesList([]);
            setLimit('0');
            setToUpdate(false);
        }
    };

    const toggleModal = (productItem, activeModal) => {
        setModal(!modal);
        setProductItem(productItem);
        setActiveModal(activeModal);
    };

    const resetFields = () => {
        setName('');
        setDescription('');
        setPrice('');
        setImage(null);
        setImageName('');

        setNameError('');
        setPriceError('');
        setDescriptionError('');
        setImageError('');
    }

    const togglePreviewModal = (productItem) => {
        setPreviewModal(!previewModal);
    };
    return (
        <>
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Add New Item</CardTitle>
                            </CardHeader>

                            {/* Variation Modal */}
                            <Modal isOpen={variationModal} toggle={toggleVariationModal}>
                                <div className="modal-header">

                                    <h5 className="modal-title text-center"
                                        id="exampleModalLabel"> Add / Edit Variation
                                </h5>
                                    <button
                                        aria-label="Close"
                                        className="close"
                                        type="button"
                                        onClick={toggleVariationModal}
                                    >
                                        <span aria-hidden={true}>×</span>
                                    </button>
                                </div>
                                <ModalBody >

                                    <Row className="no-gutters">
                                        <Col xs={6}>
                                            <FormGroup>
                                                <Label for="variation1">Variation Name</Label>
                                                <Input placeholder="Ex. Flavor" disabled={toUpdate} maxlength="25" value={variationName} onChange={handleVariationNameChange} />
                                                <small className="text-danger">  {variationNameError ? variationNameError : ''}</small>
                                            </FormGroup>
                                        </Col>
                                        <Col xs={6}>
                                            <FormGroup>
                                                <Label for="exampleSelect">Selection Type</Label>
                                                <Input type="select" name="select" id="exampleSelect" value={select} onChange={handleSelectionChange}>
                                                    <option>Single</option>
                                                    <option>Multiple</option>
                                                </Input>
                                                <Input className="pl-3 pt-2 pb-2" type="text" value={limit} onChange={handleLimitChange} placeholder="Enter limit" hidden={hideLimit}></Input>
                                                <small className="text-danger">  {selectError ? selectError : ''}</small>

                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row className="no-gutters">
                                        <Col xs={6}>
                                            <FormGroup>
                                                <Label for="variation1">Enter Choice</Label>
                                                <Input placeholder="Ex. Chocolate" value={choice} onChange={handleChoiceChange} maxlength="25" />
                                                <small className="text-danger">  {choiceError ? choiceError : ''}</small>

                                            </FormGroup>
                                        </Col>
                                        <Col xs={6}>
                                            <FormGroup>
                                                <Label for="option1">Add-On Price</Label>
                                                <Input placeholder="Ex. P100.00" value={addOnPrice} onChange={handleAddOnPriceChange} maxlength="5" />
                                                <small className="text-danger">  {addOnPriceError ? addOnPriceError : ''}</small>

                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <FormText className="mr-auto ml-auto" color="danger">Note: Must add atleast 2 choices</FormText>
                                    <Button size="sm" onClick={addChoice} className="btn btn-default pull-left"  >
                                        <i className="nc-icon nc-simple-add" aria-hidden="true" /> ADD
                                    </Button>
                                    <table className="table table-hover mb-0"  >
                                        <thead className="text-muted">
                                            <tr>
                                                <th>Choices</th>
                                                <th>Price</th>
                                                <th width='5%'></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {choicesList && choicesList.map((item) => {
                                                return (
                                                    <tr id="addr0" >
                                                        <td> {item.choice} </td>
                                                        <td> {item.price}</td>
                                                        <td>
                                                            <Button size="sm" onClick={() => removeChoice(item)} className="btn btn-default pull-left"  >
                                                                <i className="nc-icon nc-simple-delete" aria-hidden="true" />
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                    </table>
                                </ModalBody>
                                <div className="modal-footer">
                                    <Button onClick={() => addVariation()} color="success">Save</Button>
                                </div>
                            </Modal>

                            {/* Available Items Modal */}
                            <Modal isOpen={modal} toggle={toggleModal}>
                                <div className="modal-header">
                                    <h5 className="modal-title text-center" id="exampleModalLabel"> Edit  </h5>
                                    <button
                                        aria-label="Close"
                                        className="close"
                                        type="button"
                                        onClick={toggleModal}
                                    >
                                        <span aria-hidden={true}>×</span>
                                    </button>
                                </div>
                                <img width="100%" src={IMAGE_URL + user.userId + '/' + productItem.image1} alt="Card image cap" />
                                <ModalBody >
                                    <Row>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="name">Name </Label>
                                                <Input placeholder="Cake" value={productItem.name} disabled />
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="name">Price</Label>
                                                <Input placeholder="Cake" value={productItem.price} disabled />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <FormGroup>
                                                <Label for="description">Description</Label>
                                                <Input type="textarea" value={productItem.description} disabled placeholder="Food Description" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Card>
                                        <Table responsive className="table table-hover">
                                            <thead class="text-muted">
                                                <tr>
                                                    <th>Variation</th>
                                                    <th>Choices</th>
                                                    <th >Type</th>

                                                </tr>
                                            </thead>
                                            {productItem.variationList && productItem.variationList.map(variation => {
                                                return (
                                                    <tr id="addr0" >
                                                        <td> {variation.variationName} </td>
                                                        <td>
                                                            {variation.optionList.map((item, i) => {
                                                                return (
                                                                    <>
                                                                        {item.optionName} ({formatNumber(item.price)})
                                                                        {i < variation.optionList.length - 1 ? ', ' : ''}

                                                                    </>
                                                                )
                                                            })}

                                                        </td>
                                                        <td> {variation.type}</td>
                                                    </tr>
                                                )
                                            })}
                                        </Table>
                                    </Card>
                                    <br />
                                </ModalBody>
                                <div className="modal-footer">
                                    <Button onClick={() => removeProduct(productItem.productId)} color="danger" >Delete</Button>

                                    {activeModal ?
                                        <Button onClick={() => sendToOutOfStock(productItem.productId)} color="warning" >Move to Not Yet Ready</Button>
                                        :
                                        <Button onClick={() => sendToAvailableItems(productItem.productId)} color="success" >Move to Available Items</Button>
                                    }
                                </div>
                            </Modal>


                            <CardBody>
                                <Row className="no-gutters">
                                    <Col xs={6} md={6}>
                                        <FormGroup className="required">
                                            <p className="mb-1 control-label">Name </p>
                                            <Input className="has-error" placeholder="Cake" value={name} onChange={handleNameChange} maxlength="30" pattern="[A-Za-z]{3}" />
                                            <small className="text-danger">  {nameError ? nameError : ''}</small>
                                        </FormGroup>
                                    </Col>
                                    <Col xs={6} md={6}>
                                        <FormGroup>
                                            <p className="mb-1">Price </p>
                                            <Input placeholder="Price" value={price} onChange={handlePriceChange} maxlength="6" />
                                            <small className="text-danger">  {priceError ? priceError : ''}</small>

                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row className="no-gutters">
                                    <Col md={6}>
                                        <FormGroup>
                                            <p className="mb-1">Description</p>
                                            <Input type="textarea" placeholder="Food Description" value={description} onChange={handleDescriptionChange} maxlength="200" />
                                            <small className="text-danger">  {descriptionError ? descriptionError : ''}</small>
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup >
                                            <p className="mb-1" >Food Image</p>
                                            <Input className="p-1" type="file" name="file" id="exampleFile" value={imageName} onChange={handleImageChange} />
                                            <small className="text-danger">  {imageError ? imageError : ''}</small>
                                            <FormText className="p-1" color="muted">Please upload your item image in landscape orientation. Otherwise, it will be cropped into 4:3 aspect ratio</FormText>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Col>
                                </Col>
                                <Row className="no-gutters">
                                    <Col md="12">
                                        <Card>
                                            <Table className="table table-hover">
                                                <thead class="text-muted">
                                                    <tr>
                                                        <th>Variation</th>
                                                        <th>Choices</th>
                                                        <th width="10%">Type</th>
                                                        <th width="2%">
                                                            <Button size="sm" onClick={() => toggleVariationModal()} className="btn btn-default pull-right"  >
                                                                <i class="nc-icon nc-simple-add" aria-hidden="true" />
                                                            </Button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                {variationList && variationList.map((variation) => {
                                                    return (
                                                        <tr onClick={() => toggleVariationModal(variation)} id="addr0" >
                                                            <td> {variation.variation} </td>
                                                            <td>
                                                                {variation.choices.map((item, i) => {
                                                                    return (
                                                                        <>
                                                                            {item.choice}
                                                                            {i < variation.choices.length - 1 ? ', ' : ''}

                                                                        </>
                                                                    )
                                                                })}

                                                            </td>
                                                            <td> {variation.selection}</td>
                                                            <td width="2%">
                                                                <Button size="sm" onClick={() => removeVariation(variation)} className="btn btn-default"  >
                                                                    <i class="nc-icon nc-simple-delete" aria-hidden="true" />
                                                                </Button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </Table>
                                        </Card>
                                        <Button onClick={() => resetFields()} color="warning">Clear</Button>
                                        <Button color="success" onClick={save}> <i class="fa fa-floppy-o" aria-hidden="true" /> Save </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Available</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Table className="table table-hover">
                                    <thead className="text-muted">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Image</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {availableItems.map((item) => {
                                            return (

                                                <tr onClick={() => {
                                                    toggleModal(item, true);
                                                }}>
                                                    <td>{item.productId}</td>
                                                    <td>{item.name}</td>
                                                    <td>{formatNumber(item.price)}</td>
                                                    <td>
                                                        <img
                                                            alt="..."
                                                            width="50px"
                                                            className="img-rounded img-responsive"
                                                            src={IMAGE_URL + user.userId + '/' + item.image1} />
                                                    </td>
                                                    <td> </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </Table>

                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="12">
                        <Card className="card-plain">
                            <CardHeader>
                                <CardTitle tag="h4">Not yet ready</CardTitle>

                                {outOfStockItems.length == 0 ?
                                    <p className="text-muted">Not Available</p> :
                                    <p className="text-muted"> You can update these items once available</p>
                                }
                            </CardHeader>
                            <CardBody>
                                <Table className="table table-hover">
                                    <thead className="text-muted">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Image</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {outOfStockItems.map((item) => {
                                            return (

                                                <tr onClick={() => {
                                                    toggleModal(item, false);
                                                }}>
                                                    <td>{item.productId}</td>
                                                    <td>{item.name}</td>
                                                    <td>{formatNumber(item.price)}</td>
                                                    <td>
                                                        <img
                                                            alt="..."
                                                            width="50px"
                                                            className="img-rounded img-responsive"
                                                            src={IMAGE_URL + user.userId + '/' + item.image1} />
                                                    </td>
                                                    <td> </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        </>
    );
}

export default Menu;
