import React from "react";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
  FormText,
  InputGroupText,
  Label
} from "reactstrap";
import { AuthContext } from "contexts/AuthContext";
import { IMAGE_URL, ALT_IMAGE, ERROR_REQUIRED } from "constants/index"
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { uploadShopImage, getCurrentUser, updateShopInfo, uploadShopAvailability, uploadDeliveryService, updatePreparationTime } from 'helpers/API';
import InputGroup from "reactstrap/lib/InputGroup";
import InputGroupAddon from "reactstrap/lib/InputGroupAddon";

const User = () => {
  const [user, setUser] = React.useState('');
  const [fullName, setFullName] = React.useState('');
  const [userName, setUserName] = React.useState('');
  const [emailAddress, setEmailAddress] = React.useState('');
  const [shopName, setShopName] = React.useState('');
  const [contactNumber, setContactNumber] = React.useState('');
  const [street, setStreet] = React.useState('');
  const [brgy, setBrgy] = React.useState('');
  const [city, setCity] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [image, setImage] = React.useState('');
  const [imageName, setImageName] = React.useState('');
  const [readyToAccept, setReadyToAccept] = React.useState(false);
  const [hasDeliveryService, setHasDeliveryService] = React.useState(false);
  const [deliveryFee, setDeliveryFee] = React.useState('');
  const [deliveryFeeError, setDeliveryFeeError] = React.useState('');
  const [currentPassword, setCurrentPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [preparationTime, setPreparationTime] = React.useState('30');

  const handleFullNameChange = (event) => {
    setFullName(event.target.value);
  };
  const handleUserNameChange = (event) => {
    setUserName(event.target.value);
  };
  const handleEmailChange = (event) => {
    setEmailAddress(event.target.value);
  };
  const handleShopNameChange = (event) => {
    setShopName(event.target.value);
  };
  const handleContactNumberChange = (event) => {
    setContactNumber(event.target.value);
  };
  const handleStreetChange = (event) => {
    setStreet(event.target.value);
  };
  const handleBrgyChange = (event) => {
    setBrgy(event.target.value);
  };
  const handleCityChange = (event) => {
    setCity(event.target.value);
  };
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };
  const handleImageChange = (event) => {
    setImage(event.target.files[0]);
    setImageName(event.target.value);
  };
  const handleCurrentPasswordChange = (event) => {
    setCurrentPassword(event.target.value);
  };
  const handleNewPasswordChange = (event) => {
    setNewPassword(event.target.value);
  };
  const handleDeliveryFeeChange = (event) => {
    setDeliveryFee(event.target.value.replace(/[^\d+\.?\d*]/, ''));
    setDeliveryFeeError(event.target.value.length > 0 ? '' : ERROR_REQUIRED)
  };

  const handlePreparationTimeChange = (event) => {
    setPreparationTime(event.target.value);
  
    updatePreparationTime(JSON.stringify(preparationTime)).then(res => {
      alert('Successfully updated!');

      refreshData();
    });
  }
  const handleHasDeliveryServiceChange = () => {
    if (user.hasOwnDelivery === true) {
      confirmAlert({
        customUI: ({ onClose }) => {
          return (
            <Col xs="12" className="mr-auto mx-auto">
              <Card className="card-plain">
                <h5>Do you want to remove your delivery service?</h5>
                <CardFooter className="mr-auto mx-auto p-4">
                  <Button onClick={() => onClose()} color="warning">Cancel</Button>
                  <Button onClick={() => { removeDeliveryService(); onClose() }} color="success">Yes</Button>
                </CardFooter>
              </Card>
            </Col>

          );
        }
      });
    } else {
      setHasDeliveryService(!hasDeliveryService);
      setDeliveryFee('');
    }
  };
  const confirmReadyToAccept = () => {
    uploadShopAvailability(!readyToAccept).then(res => {
      setReadyToAccept(!readyToAccept);
    });
  };
  const confirmDeliveryService = () => {

    if (deliveryFee) {
      const body = {
        hasOwnDelivery: true,
        deliveryFee: deliveryFee
      }
      uploadDeliveryService(JSON.stringify(body)).then(res => {
        alert('Successfully updated!');
        refreshData();
      });
    } else {
      setDeliveryFeeError(ERROR_REQUIRED);
    }

  };
  const removeDeliveryService = () => {
    const body = {
      hasOwnDelivery: false,
      deliveryFee: null
    }

    uploadDeliveryService(JSON.stringify(body)).then(res => {
      refreshData();
      setDeliveryFeeError('');

    });
  };

  const handleReadyToAcceptChange = () => {
    confirmAlert({
      customUI: ({ onClose }) => {

        return (
          <Col xs="9" className="mr-auto mx-auto">
            <Card className="card-plain">
              {!readyToAccept ?
                <h5>Are you sure you're ready to accept orders?</h5>
                :
                <h5>You're closing your shop already?</h5>
              }
              <CardFooter className="mr-auto mx-auto p-4">
                <Button onClick={() => onClose()} color="warning">Not yet</Button>
                <Button onClick={() => { confirmReadyToAccept(); onClose() }} color="success">Yep!</Button>
              </CardFooter>
            </Card>
          </Col>

        );
      }
    });
  };

  const uploadImage = () => {
    let formData = new FormData();
    formData.append('image', image);

    uploadShopImage(formData).then(res => {
      alert('Successfully updated!');
      refreshData();
    }).catch(function() {
      alert("We are unable to upload your image. It may be too small or not landscape-oriented.")
  });
    



  }
  const update = () => {

    const body = {
      'userId': user.userId,
      'shopName': shopName,
      'userName': userName,
      'emailAddress': emailAddress,
      'description': description,
      'isReadyToAcceptOrder': readyToAccept,
      'hasOwnDelivery': hasDeliveryService,
      'ownerName': fullName,
      'contactNumber': contactNumber,
      'street': street,
      'brgy': brgy,
      'city': city,
      'currentPassword': currentPassword,
      'newPassword': newPassword,
    };

    updateShopInfo(JSON.stringify(body)).then(res => {
      console.log('Successfully updated!');
    },
      error => {
        alert(error);
      });


  }

  const refreshData = () => {
    getCurrentUser().then(response => {
      const user = response;
      setUser(user);
      setShopName(user.shopName ? user.shopName : '');
      setEmailAddress(user.emailAddress ? user.emailAddress : '');
      setUserName(user.userName ? user.userName : '');
      setFullName(user.ownerName ? user.ownerName : '');
      setContactNumber(user.contactNumber ? user.contactNumber : '');
      setStreet(user.street ? user.street : '');
      setBrgy(user.brgy ? user.brgy : '');
      setCity(user.city ? user.city : '');
      setDescription(user.description ? user.description : '');
      setHasDeliveryService(user.hasOwnDelivery ? user.hasOwnDelivery : false);
      setReadyToAccept(user.isReadyToAcceptOrder ? user.isReadyToAcceptOrder : false);
      setDeliveryFee(user.deliveryFee ? user.deliveryFee : '');

    })
  }

  React.useEffect(() => {
    refreshData();
  }, []);

  return (
    <>
      <div className="content">
        <Row>
          <Col md="4">
            <Card className="card-user">
              <CardBody>

                <img width="100%" src={IMAGE_URL + user.userId + '/' + user.imageName} alt="Card image cap" />
                <FormText>Please upload good quality image with an aspect ratio of 4:3 in landscape orientation and should be no more than 10 MB</FormText>
                <Row>
                  <Col xs="6">
                    <Input type="file" className="mt-1" name="file" id="exampleFile" value={imageName} onChange={handleImageChange} />
                  </Col>
                  <Col xs="6">
                    <Button size="sm" className="btn btn-default pull-right mt-1" onClick={() => uploadImage()} > Upload  </Button>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Row>
                  <Col md="12">
                    <FormGroup >
                      <Label for="dropdown">Preparation Time <small>(Including delivery)</small></Label>
                      <Input type="select" name="select" value={preparationTime} onChange={handlePreparationTimeChange}  >
                        <option name="preptime" value="30">30 Minutes</option>
                        <option name="preptime" value="45">45 Minutes</option>
                        <option name="preptime" value="60">60 Minutes</option>
                      </Input>
                      <p className="text-muted mb-0">
                        <input onClick={handleReadyToAcceptChange} className="align-middle" type="checkbox" checked={readyToAccept} />
                        &nbsp;My shop is ready to accept orders </p>
                      <p className="text-muted mb-0"> <input onClick={handleHasDeliveryServiceChange} className="align-middle" type="checkbox" checked={hasDeliveryService} />
                       &nbsp;I have my own delivery service  </p>

                      {hasDeliveryService ?
                        <>

                          <Label>Delivery Fee (PHP)<small className="align-middle "> </small></Label>
                          <Input size="sm" placeholder="Enter Delivery Fee" type="text" value={deliveryFee} onChange={handleDeliveryFeeChange} />

                          <small className="text-danger">  {deliveryFeeError ? deliveryFeeError : ''}</small>
                          <Button size="sm" className="btn btn-default pull-right" onClick={() => confirmDeliveryService()} > Update  </Button>
                        </>
                        : ''
                      }

                    </FormGroup>

                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
          <Col md="8">
            <Card className="card-user">
              <CardHeader>
                <CardTitle tag="h5">Edit Shop</CardTitle>
              </CardHeader>
              <CardBody>
                <Form>
                  <Row className="mb-0">
                    <Col md="6">
                      <FormGroup>
                        <label>Shop Name</label>
                        <Input placeholder="Ex. Cake Shop" type="text" value={shopName} onChange={handleShopNameChange} />
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <label>Username</label>
                        <Input type="text" disabled value={userName} onChange={handleUserNameChange} />
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <label htmlFor="exampleInputEmail1">
                          Email address
                          </label>
                        <Input placeholder="Email" type="email" value={emailAddress} onChange={handleEmailChange} disabled />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <label>Owner Name</label>
                        <Input placeholder="Owner Name" type="text" value={fullName} onChange={handleFullNameChange} />
                      </FormGroup>
                    </Col>
                    <Col md="6">
                      <FormGroup>
                        <label>Contact Number</label>
                        <Input placeholder="Last Name" type="text" value={contactNumber} onChange={handleContactNumberChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <label>Street (Include Landmark) </label>
                        <Input placeholder="Street" type="text" value={street} onChange={handleStreetChange} />
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <label>Brgy</label>
                        <Input placeholder="Brgy" type="text" value={brgy} onChange={handleBrgyChange} />
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <label>City</label>
                        <Input placeholder="City" type="input" value={city} onChange={handleCityChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <label>Description</label>
                        <Input placeholder="Describe your shop" type="textarea" value={description} onChange={handleDescriptionChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <label>Current Password</label>
                        <Input type="password" value={currentPassword} onChange={handleCurrentPasswordChange} />
                      </FormGroup>
                    </Col>
                    <Col md="6">
                      <FormGroup>
                        <label>New Password</label>
                        <Input type="password" value={newPassword} onChange={handleNewPasswordChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <div className="update ml-auto mr-auto">
                      <Button onClick={() => update()} className="btn-round" color="success" type="submit" >
                        Save
                        </Button>
                    </div>
                  </Row>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );

}

export default User;
