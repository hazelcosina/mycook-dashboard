import React from "react";

import { Line, Pie } from "react-chartjs-2";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Table
} from "reactstrap";
import { getLatestUpdate, generateReport, downloadReport } from 'helpers/API';
import { AuthContext } from "contexts/AuthContext";
import { formatNumber, displayDate, convertToTime } from '../helpers/utils';
import moment from 'moment';

import {
  dashboard24HoursPerformanceChart,
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart,
} from "variables/charts.js";

const Dashboard = () => {
  const { user } = React.useContext(AuthContext);
  const [completedCount, setCompletedCount] = React.useState(0);
  const [totalSales, setTotalSales] = React.useState(0);
  const [totalAdminFee, setTotalAdminFee] = React.useState(0);
  const [newOrdersCount, setNewOrdersCount] = React.useState(0);
  const [reportList, setReportList] = React.useState([]);

  React.useEffect(() => {
    getLatest();
  }, []);

  const getLatest = () => {

    const format = 'YYYYMMDD';
    let currentDate = moment().format(format);
    getLatestUpdate(user.userId, currentDate).then(data => {
      if (data) {
        setCompletedCount(data.completedCount);
        setNewOrdersCount(data.newOrdersCount);
        setTotalSales(data.sales);
        setReportList(data.reportList);
        setTotalAdminFee(data.webFee);
      } else {
        console.log('Error retrieving data')
      }
    })
  }

  const generateToday = () => {
    const format = 'YYYYMMDD';
    let currentDate = moment().format(format);
    const body = JSON.stringify(currentDate);

    generateReport(user.userId, body).then(res => {
      console.log('Succesfully generated')
      getLatest();
    })
  }

  const download = (date) => {
    downloadReport(user.userId, date).then(res => {
      console.log('Succesfully generated')
    })
  }
  return (
    <>
      <div className="content">
        <Row>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-money-coins text-primary" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Total Sales</p>
                      <CardTitle tag="p">{formatNumber(totalSales)}</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-alert-circle-i text-warning" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">New Orders</p>
                      <CardTitle tag="p">{newOrdersCount}</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-favourite-28 text-danger" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Completed</p>
                      <CardTitle tag="p">{completedCount}</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-app text-success" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Web Admin Fee</p>
                      <CardTitle tag="p">{formatNumber(totalAdminFee)}</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <Button onClick={() => generateToday()} className="pull-right" color="default">Generate Today</Button>

                <CardTitle tag="h5">Reports</CardTitle>

              </CardHeader>
              <CardBody>
                <Table responsive className="table table-hover text-center">
                  <thead className="text-muted">
                    <th>Download</th>
                    <th>Date</th>
                    <th>Time Generated</th>
                    <th>Total Sales</th>
                    <th>Completed Orders</th>
                    <th>Web Fee</th>
                  </thead>
                  {reportList && reportList.map((report, index) => {
                    return (
                      <tbody>
                        <tr>
                          <td><Button onClick={() => download(report.date)} color="success"><i className="nc-icon nc-cloud-download-93 align-middle" /></Button></td>
                          <td> {displayDate(report.date)} </td>
                          <td> {report.generatedTime} </td>
                          <td> {formatNumber(report.totalSales)} </td>
                          <td> {report.completedOrdersCount} </td>
                          <td>{formatNumber(report.webAdminFee)}</td>
                        </tr>

                      </tbody>)
                  })}
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    </>
  );
}


export default Dashboard;
