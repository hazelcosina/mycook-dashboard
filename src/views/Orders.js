import React from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  FormText,
  Label
} from "reactstrap";

import { getAllOrders, confirmOrder, markOrderAsComplete, inTransitOrder, deliverOrder, rejectOrder } from 'helpers/API';
import { AuthContext } from "contexts/AuthContext";
import { formatNumber, convertToDate, convertToTime } from '../helpers/utils';
import ModalHeader from "reactstrap/lib/ModalHeader";
import { IMAGE_URL } from "constants/index"
import moment from 'moment';
import OrderDetail from "components/Order/OrderDetail";

const Orders = (props) => {
  const { user } = React.useContext(AuthContext);
  const [order, selectedOrder] = React.useState({});

  const [activeOrders, setActiveOrders] = React.useState([]);
  const [acceptedOrders, setAcceptedOrders] = React.useState([]);
  const [pendingDeliveries, setPendingDelivery] = React.useState([]);
  const [inTransitOrders, setInTransitOrders] = React.useState([]);
  const [completedOrders, setCompletedOrders] = React.useState([]);

  const [confirmationModal, setConfirmationModal] = React.useState(false);
  const [acceptedModal, setAcceptedModal] = React.useState(false);
  const [deliveryModal, setDeliveryModal] = React.useState(false);
  const [inTransitModal, setInTransitModal] = React.useState(false);
  const [completedModal, setCompletedModal] = React.useState(false);


  React.useEffect(() => {
    loadOrders();
  }, []);


  const loadOrders = () => {
    const format = 'YYYYMMDD';
    let currentDate = moment().format(format);

    getAllOrders(user.userId, currentDate).then(res => {
      if (res) {
        const newOrders = res.filter(order => (order.status === 'New'));
        setActiveOrders(newOrders);

        const accepted = res.filter(order => (order.status === 'Accepted'));
        setAcceptedOrders(accepted);

        const delivery = res.filter(order => (order.status === 'Delivery'));
        setPendingDelivery(delivery);

        const intransit = res.filter(order => (order.status === 'InTransit'));
        setInTransitOrders(intransit);

        const completedOrders = res.filter(order => (order.status === 'Completed'));
        setCompletedOrders(completedOrders);
      }
    });
  }

  const toggleForConfirmation = (order) => {
    setConfirmationModal(!confirmationModal);
    selectedOrder(order);
  };

  const toggleForAccepted = (order) => {
    setAcceptedModal(!acceptedModal);
    selectedOrder(order);
  };

  const toggleForDelivery = (order) => {
    setDeliveryModal(!deliveryModal);
    selectedOrder(order);
  };
  const toggleForInTransit = (order) => {
    setInTransitModal(!inTransitModal);
    selectedOrder(order);
  };

  const toggleForCompleted = (order) => {
    setCompletedModal(!completedModal);
    selectedOrder(order);
  };

  const confirm = (order) => {
    // Setting order to Accept status
    const body = JSON.stringify(order);
    confirmOrder(body).then(res => {
      // alert('You have confirmed reference number ' + order.referenceNumber + '. Please prepare order and get it delivered on schedule.')

      loadOrders();
      toggleForConfirmation(order)
    })
  }

  const moveToDelivery = (order) => {
    // Setting order to Delivery Status
    const body = JSON.stringify(order);
    deliverOrder(body).then(res => {
      loadOrders();
      toggleForAccepted(order)
    })
  }

  const moveToInTransit = (order) => {
    // Setting order to In Transit Status
    const body = JSON.stringify(order);
    inTransitOrder(body).then(res => {
      loadOrders();
      toggleForDelivery(order)
    })
  }

  const markAsComplete = (order) => {
    // Setting order to Complete Status
    const body = JSON.stringify(order);
    markOrderAsComplete(body).then(res => {
      loadOrders();
      toggleForInTransit(order)
    })
  }

  const denyOrder = (order) => {
    // Setting order to Complete Status
    const body = JSON.stringify(order);
    rejectOrder(body).then(res => {
      loadOrders();
      toggleForConfirmation(order)
    })
  }

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">({activeOrders.length}) For Confirmation</CardTitle>
              </CardHeader>
              <CardBody>
                <Modal className="mt-5" isOpen={confirmationModal} toggle={toggleForConfirmation}>
                  <ModalHeader toggle={toggleForConfirmation} charCode="x">For Confirmation</ModalHeader>
                  <ModalBody >
                    <OrderDetail order={order} />
                  </ModalBody>
                  <div className="modal-footer">
                    <Button onClick={() => denyOrder(order)} className="btn-round" color="danger" type="button">  Deny </Button>
                    <Button onClick={() => confirm(order)} className="btn-round" color="success" type="button"> Accept </Button>
                  </div>
                </Modal>
                <Table responsive className="table table-hover">
                  <thead className="text-default">
                    <tr>
                      <th>Action</th>
                      <th>Orders</th>
                      <th>Schedule Date</th>
                      <th>Name</th>
                      <th>Delivery Address</th>
                      <th>Total Payment</th>
                      <th>Order ID</th>
                    </tr>
                  </thead>
                  <tbody>
                    {activeOrders && activeOrders.map((order, index) => {
                      return (
                        <tr onClick={() => {
                          toggleForConfirmation(order);
                        }}>
                          <td>
                            <Button size="sm" color="danger"><i className="nc-icon nc-alert-circle-i"></i> </Button>
                          </td>
                          <td> ({order.orderedItems.length}) Item/s </td>
                          <td> {order.scheduledDate} </td>
                          <td> {order.customer.fullName} </td>
                          <td>
                            {order.customer && order.customer.address ?
                              order.customer.address.street + ', ' +
                              order.customer.address.brgy + ', ' +
                              order.customer.address.city : ''}
                          </td>
                          <td>{formatNumber(order.totalPayment)}</td>
                          <td>{order.referenceNumber}</td>
                        </tr>
                      )
                    })
                    }
                  </tbody>
                  {activeOrders.length == 0 ?
                    <FormText>  No pending confirmation </FormText>
                    :
                    <></>
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>

          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">({acceptedOrders.length}) Currently preparing</CardTitle>
              </CardHeader>
              <CardBody>
                <Modal className="mt-5" isOpen={acceptedModal} toggle={toggleForAccepted}>
                  <ModalHeader toggle={toggleForAccepted} charCode="x">In progress</ModalHeader>
                  <ModalBody >
                    <OrderDetail order={order} />
                  </ModalBody>
                  <div className="modal-footer">
                    <Button onClick={toggleForAccepted} className="btn-round" color="warning" type="button"> Back </Button>
                    <Button onClick={() => moveToDelivery(order)} className="btn-round" color="success" type="button"> Send for delivery </Button>
                  </div>
                </Modal>
                <Table responsive className="table table-hover">
                  <thead className="text-default">
                    <tr>
                      <th>Action</th>
                      <th>Orders</th>
                      <th>Schedule Date</th>
                      <th>Name</th>
                      <th>Delivery Address</th>
                      <th>Total Payment</th>
                      <th>Order ID</th>
                    </tr>
                  </thead>
                  {acceptedOrders && acceptedOrders.map((order, index) => {
                    return (
                      <tbody>
                        <tr onClick={() => {
                          toggleForAccepted(order);
                        }}>
                          <td>
                            <Button size="sm" color="danger"><i className="nc-icon nc-alert-circle-i"></i> </Button>
                          </td>
                          <td> ({order.orderedItems.length}) Item/s </td>
                          <td> {order.scheduledDate} </td>
                          <td> {order.customer.fullName} </td>
                          <td>
                            {order.customer && order.customer.address ?
                              order.customer.address.street + ', ' +
                              order.customer.address.brgy + ', ' +
                              order.customer.address.city : ''}
                          </td>
                          <td>{formatNumber(order.totalPayment)}</td>
                          <td>{order.referenceNumber}</td>
                        </tr>

                      </tbody>)
                  })
                  }
                  {acceptedOrders.length == 0 ?
                    <FormText>  No order is being prepared</FormText>
                    :
                    <></>
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">({pendingDeliveries.length}) Waiting to be delivered</CardTitle>
              </CardHeader>
              <CardBody>
                <Modal className="mt-5" isOpen={deliveryModal} toggle={toggleForDelivery}>
                  <ModalHeader toggle={toggleForDelivery} charCode="x">Awaiting Delivery</ModalHeader>
                  <ModalBody >
                    <OrderDetail order={order} />
                    <FormText className="text-danger"><b>Scheduled delivery on {order.scheduledDate} at {order.scheduledTime}</b></FormText>

                  </ModalBody>
                  <div className="modal-footer">
                    <Button onClick={toggleForDelivery} className="btn-round" color="danger" type="button">  Back </Button>
                    {order.shop && order.shop.deliveryService ?
                      <Button onClick={() => moveToInTransit(order)} className="btn-round" color="success" type="button"> In Transit </Button>
                      :
                      ''
                    }
                  </div>
                </Modal>
                <Table responsive className="table table-hover">
                  <thead className="text-default">
                    <tr>
                      <th>Action</th>
                      <th>Orders</th>
                      <th>Schedule Date</th>
                      <th>Name</th>
                      <th>Delivery Address</th>
                      <th>Total Payment</th>
                      <th>Order ID</th>
                    </tr>
                  </thead>
                  <tbody>

                    {pendingDeliveries && pendingDeliveries.map((order, index) => {
                      return (
                        <tr onClick={() => {
                          toggleForDelivery(order);
                        }}>
                          <td>
                            <Button size="sm" color="danger"><i className="nc-icon nc-alert-circle-i"></i> </Button>
                          </td>
                          <td> ({order.orderedItems.length}) Item/s </td>
                          <td> {order.scheduledDate} </td>
                          <td> {order.customer.fullName} </td>
                          <td>
                            {order.customer && order.customer.address ?
                              order.customer.address.street + ', ' +
                              order.customer.address.brgy + ', ' +
                              order.customer.address.city : ''}
                          </td>
                          <td>{formatNumber(order.totalPayment)}</td>
                          <td>{order.referenceNumber}</td>
                        </tr>
                      )
                    })
                    }
                  </tbody>
                  {pendingDeliveries.length == 0 ?
                    <FormText>  No pending delivery  </FormText>
                    :
                    <></>
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">({inTransitOrders.length}) Currently In Transit </CardTitle>
              </CardHeader>
              <CardBody>
                <Modal className="mt-5" isOpen={inTransitModal} toggle={toggleForInTransit}>
                  <ModalHeader toggle={toggleForInTransit} charCode="x">In Transit</ModalHeader>
                  <ModalBody >
                    <OrderDetail order={order} />
                    {
                      order.shop && order.shop.deliveryService ?
                        <>
                          <hr />
                          <h6>Rider Details</h6>
                          <Row className="no-gutters">
                            <Col lg="6" md="6" xs="6"><small> Rider Name</small></Col>
                            <Col lg="6" md="6" xs="6"><small> {order.rider ? order.rider.riderName : ''} </small></Col>
                          </Row>
                          <Row className="no-gutters">
                            <Col lg="6" md="6" xs="6"><small> Contact Number</small></Col>
                            <Col lg="6" md="6" xs="6"><small> +639 {order.rider ? order.rider.contactNumber : ''} </small></Col>
                          </Row>
                        </> : ''
                    }
                  </ModalBody>
                  <div className="modal-footer">
                    <Button onClick={toggleForInTransit} className="btn-round" color="danger" type="button">  Back </Button>

                    {
                      order.shop && order.shop.deliveryService ?
                        <Button onClick={() => markAsComplete(order)} className="btn-round" color="success" type="button"> Arrived </Button>
                        : ''}
                  </div>
                </Modal>
                <Table responsive className="table table-hover">
                  <thead className="text-default">
                    <tr>
                      <th>Action</th>
                      <th>Orders</th>
                      <th>Schedule Date</th>
                      <th>Name</th>
                      <th>Delivery Address</th>
                      <th>Total Payment</th>
                      <th>Order ID</th>
                    </tr>
                  </thead>
                  <tbody>

                    {inTransitOrders && inTransitOrders.map((order, index) => {
                      return (
                        <tr onClick={() => {
                          toggleForInTransit(order);
                        }}>
                          <td>
                            <Button size="sm" color="danger"><i className="nc-icon nc-alert-circle-i"></i> </Button>
                          </td>
                          <td> ({order.orderedItems.length}) Item/s </td>
                          <td> {order.scheduledDate} </td>
                          <td> {order.customer.fullName} </td>
                          <td>
                            {order.customer && order.customer.address ?
                              order.customer.address.street + ', ' +
                              order.customer.address.brgy + ', ' +
                              order.customer.address.city : ''}
                          </td>
                          <td>{formatNumber(order.totalPayment)}</td>
                          <td>{order.referenceNumber}</td>
                        </tr>
                      )
                    })
                    }
                  </tbody>
                  {inTransitOrders.length == 0 ?
                    <FormText>  No in transit orders  </FormText>
                    :
                    <></>
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col md="12">
            <Card className="card-plain">
              <CardHeader>
                <CardTitle tag="h4">({completedOrders.length}) Completed</CardTitle>
                {completedOrders.length == 0 ?
                  <FormText>  No completed orders yet. </FormText> : <></>
                }
              </CardHeader>
              <CardBody>
                <Modal className="mt-5" isOpen={completedModal} toggle={toggleForCompleted}>
                  <ModalHeader toggle={toggleForCompleted} charCode="x">Completed</ModalHeader>
                  <ModalBody >
                    <OrderDetail order={order} />
                  </ModalBody>
                  <div className="modal-footer">
                    <Button onClick={toggleForCompleted} className="btn-round" color="danger" type="button"> Back </Button>
                  </div>
                </Modal>
                <Table responsive>
                  <thead className="text-default">
                    <tr>
                      <th>Action</th>
                      <th>Orders</th>
                      <th>Schedule Date</th>
                      <th>Name</th>
                      <th>Delivery Address</th>
                      <th>Total Payment</th>
                      <th>Order ID</th>
                    </tr>
                  </thead>
                  {completedOrders && completedOrders.map((order, index) => {
                    return (
                      <tbody>
                        <tr onClick={() => {
                          toggleForCompleted(order);
                        }}>
                          <td>
                            <Button size="sm" color="danger"><i className="nc-icon nc-alert-circle-i"></i> </Button>
                          </td>
                          <td> ({order.orderedItems.length}) Item/s </td>
                          <td> {order.scheduledDate} </td>
                          <td> {order.customer.fullName} </td>
                          <td>
                            {order.customer && order.customer.address ?
                              order.customer.address.street + ', ' +
                              order.customer.address.brgy + ', ' +
                              order.customer.address.city : ''}
                          </td>
                          <td>{formatNumber(order.totalPayment)}</td>
                          <td>{order.referenceNumber}</td>
                        </tr>

                      </tbody>
                    )
                  })
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default Orders;
