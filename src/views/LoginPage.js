import React from "react";
import { useHistory } from "react-router-dom";
import { login, getCurrentUser } from 'helpers/API';
import { GOOGLE_AUTH_URL, OAUTH2_REDIRECT_URI, FACEBOOK_AUTH_URL, GITHUB_AUTH_URL, ACCESS_TOKEN } from '../constants';

import { Button, Card, Form, Input, InputGroup, Container, Row, Col } from "reactstrap";
import { AuthContext } from "contexts/AuthContext";
import logo from "assets/img/mycook-2.png";

const LoginPage = (props) => {
  const { success, isAuthUser, error, setLoader, logout } = React.useContext(AuthContext);
  const [userName, setUserName] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [oAuthRedirect, setoAuthRedirect] = React.useState("");
  const history = useHistory();


  const handleUserNameChange = event => {
    setUserName(event.target.value);
  }

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  }

  const handleFormSubmit = event => {
    event.preventDefault();

    const user_object = {
      email: userName,
      username: userName,
      role: 'PARTNER',
      password: password
    };

    login(user_object)
    .then(response => {
        localStorage.setItem(ACCESS_TOKEN, response.accessToken);
        handleDashboard();
    }).catch(error => {
        alert((error && error.message) || 'Oops! Something went wrong. Please try again!');
    });
  }

  const handleDashboard = () => {
    let redirect = !!props.location.state ? props.location.state.from : '/admin';
     getCurrentUser().then(response => {
       success(response);
       history.push('/admin');
     })
  }


  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    let redirect = !!props.location.state ? props.location.state.from : '/';
    if(isAuthUser){
      history.push('/admin/dashboard');
    }
    setoAuthRedirect(redirect);
  });

  return (
    <>
      <div className="main ps">
        <br /><br/><br/><br/>
        <div className="section">
          <Col className="m-auto" sm="12" lg="8" md="8">
            
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card card-body mt-3 ml-auto mr-auto bg-light text-dark">
                <img alt="..." className="mx-auto" width="50%" src={logo} />
                <b  className="mx-auto text-muted">Timmy Merchant</b>
                <small className="mx-auto">Login Your Merchant Credentials   </small>
              <br/>
                <Form >
                  <label>Username</label>
                  <Input placeholder="Username" name="username" type="text" onChange={handleUserNameChange} />
                  <label>Password</label>
                  <Input placeholder="Password" name="password" type="password" onChange={handlePasswordChange} />
                  <br />
                  <Button onClick={handleFormSubmit} block className="btn-round" color="success">
                    Log In
                  </Button>
                </Form>
                <Button
                  className="btn-link text-muted"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                >
                  Forgot password?
                  </Button>
              </Card>              
            </Col>
          </Col>
        </div>

      </div>
    </>
  );
}

export default LoginPage;
