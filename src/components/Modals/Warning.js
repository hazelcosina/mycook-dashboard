import React from "react";

import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Table,
    Row,
    Col,
    Button,
    Modal,
    ModalBody,
    FormText,
    Label,
    Input,
    FormGroup,
    Form,
    ModalHeader
} from "reactstrap";

import ModalFooter from "reactstrap/lib/ModalFooter";


const Warning = (msg) => {

    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title text-center"
                    id="exampleModalLabel">  </h5>
                <button
                    aria-label="Close"
                    className="close"
                    type="button">
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <ModalBody >
                <h5>Are you sure you want to continue?</h5>
            </ModalBody>
            <ModalFooter>
                <Button color="danger" >Yes</Button>
            </ModalFooter>
        </>
    );
}

export default Warning;
