import React from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  FormText,
  Label
} from "reactstrap";

import { login, getAllOrders, confirmOrder, markOrderAsComplete, deliverOrder } from 'helpers/API';
import { AuthContext } from "contexts/AuthContext";
import { formatNumber, convertToDate, convertToTime } from 'helpers/utils';
import ModalHeader from "reactstrap/lib/ModalHeader";
import { IMAGE_URL } from "constants/index"
import moment from 'moment';

const OrderDetail = ({ order }) => {
  const { user } = React.useContext(AuthContext);


  return (
    <>
      <div className="content">
        <h6 className="mb-3">Order Details</h6>

        <ul className="list-unstyled listing">
          {order.orderedItems && order.orderedItems.map((orderItem, i) => {
            const products = Object.keys(orderItem);
            return (
              <li>
                <Row>
                  {products.map((product) => {
                    const addOns = orderItem[product] ? orderItem[product]['addOns'] : [];
                    return (
                      <>
                        <Col className="ml-auto mr-auto" lg="3" md="3" xs="3">
                          <img
                            alt="..."
                            className="img-rounded img-responsive"
                            src={IMAGE_URL + user.userId + '/' + orderItem[product]['image']} />
                        </Col>
                        <Col className="ml-auto mr-auto p-0" lg="9" md="9" xs="9">
                          <ul className="list-unstyled listing">
                            <small>
                              <li>( {orderItem[product]['quantity']} ) x {product} </li>
                              <li>
                                <Row>
                                  {addOns.map((variation) => {
                                    return (
                                      <>
                                        <Col xs="7"> {variation.name} : {variation.value} &nbsp; </Col>
                                        <Col xs="3">  {formatNumber(variation.price)}  </Col>
                                      </>
                                    )
                                  })}
                                  <Col xs="7"> Item Price : </Col>
                                  <Col xs="3"> <Label color="warning" className="label label-warning"> {formatNumber(orderItem[product]['totalItemPrice'])}</Label></Col>
                                </Row>
                              </li>
                            </small>
                          </ul>
                        </Col>
                      </>
                    )
                  })}
                </Row>
                <hr />
              </li>
            )
          })}
        </ul>
        <Row className="no-gutters">
          <Col lg="9" md="9" xs="9">Grand Total</Col>
          <Col lg="3" md="3" xs="3"> {formatNumber(order.totalPayment)}</Col>
        </Row>
        <Row className="no-gutters">
          <Col lg="9" md="9" xs="9"><small> Delivery Option </small></Col>
          <Col lg="3" md="3" xs="3"><small>For {order.deliveryOption} </small></Col>
        </Row>
        <Row className="no-gutters">
          <Col lg="9" md="9" xs="9"><small> Delivery Fee</small></Col>
          <Col className="align-right" lg="3" md="3" xs="3"> <small> {formatNumber(order.deliveryFee)} </small></Col>
        </Row>
        <Row className="no-gutters">
          <Col lg="9" md="9" xs="9"><small> Discount </small></Col>
          <Col className="align-right" lg="3" md="3" xs="3"> <small> % {order.discountPercentage} </small></Col>
        </Row>
        <hr />
        <h6>Customer Details</h6>
        <Row className="no-gutters">
          <Col lg="6" md="6" xs="6"><small> Contact Name</small></Col>
          <Col lg="6" md="6" xs="6"><small> {order.customer ? order.customer.fullName : ''} </small></Col>
        </Row>
        <Row className="no-gutters">
          <Col lg="6" md="6" xs="6"><small> Contact Number</small></Col>
          <Col lg="6" md="6" xs="6"><small> +639 {order.customer ? order.customer.contactNumber : ''} </small></Col>
        </Row>
        <Row className="no-gutters">
          <Col lg="6" md="6" xs="6"><small> Delivery Address</small></Col>
          <Col lg="6" md="6" xs="6"><small> {order.customer && order.customer.address ?
            order.customer.address.street + ', ' +
            order.customer.address.brgy + ', ' +
            order.customer.address.city
            : ''} </small></Col>
        </Row>
      </div>
    </>
  );
}

export default OrderDetail;
