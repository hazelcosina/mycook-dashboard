import Dashboard from "views/Dashboard.js";
import Icons from "views/Icons.js";
import Orders from "views/Orders.js";
import UserPage from "views/User.js";
import Menu from "views/Menu";
import LoginPage from "views/LoginPage";

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-shop",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/orders",
    name: "Orders",
    icon: "nc-icon nc-cart-simple",
    component: Orders,
    layout: "/admin",
  },
  {
    path: "/menu",
    name: "Menu",
    icon: "nc-icon nc-paper",
    component: Menu,
    layout: "/admin",
  },
  {
    path: "/user-page",
    name: "Settings",
    icon: "nc-icon nc-settings",
    component: UserPage,
    layout: "/admin",
  }
];
export default routes;
