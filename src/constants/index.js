export const API_BASE_URL = 'http://localhost:8081';
export const ACCESS_TOKEN = 'accessToken2';
export const IMAGE_URL = '/assets/img/shops/';
export const ALT_IMAGE = '/assets/img/shops/';

export const OAUTH2_REDIRECT_URI = 'http://localhost:3000/oauth2/redirect?path='

export const GOOGLE_AUTH_URL = API_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = API_BASE_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=';

export const ERROR_REQUIRED = '* Required Field';
