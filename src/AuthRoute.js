import { AuthContext } from "contexts/AuthContext";
import React from "react";
import { Redirect, Route } from "react-router";
import AdminLayout from "layouts/Admin.js";

const AuthRoute = props => {
  const { type } = props;
  const { isAuthUser } = React.useContext(AuthContext);

 if (type == "private" && !isAuthUser) {
    return <Redirect to="/login"/>;
  }

  return <Route {...props} />;
};

export default AuthRoute;