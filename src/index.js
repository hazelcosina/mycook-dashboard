import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { AuthContext } from "contexts/AuthContext";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.2.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import AuthContextProvider from "contexts/AuthContext";
import AuthRoute from "./AuthRoute";
import LoginPage from "views/LoginPage";
import AdminLayout from "layouts/Admin.js";

const hist = createBrowserHistory();

ReactDOM.render(
  <AuthContextProvider>

  <Router history={hist}>
    <Switch>
    <Route path="/shops"  />
        <Route path="/login" render={(props) => <LoginPage {...props} />} />
        <AuthRoute type="private" path="/admin" render={(props) => <AdminLayout {...props} />} />
        <Redirect to="/login" />
    </Switch>
  </Router>
  </AuthContextProvider>

  ,
  document.getElementById("root")
);
