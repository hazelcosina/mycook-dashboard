const path = require('path');
const express = require('express');
const app = express();
const port = process.env.PORT || 3001;
const publicPath = path.join('mycook', '..', 'build');

app.use(express.static(publicPath));

app.get('*', (req, res) => {
   res.sendFile('build/index.html', {root: __dirname});
});

app.listen(port, () => {
   console.log('Server is up!');
});